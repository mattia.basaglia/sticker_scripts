import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie.objects.bezier import Bezier
from lottie import Point
import random

name = "flag"
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

flag = animation.find("flag")
animation.find("flag w").transform.rotation.value = 25

sine_displacer = anutils.SineDisplacer(300, 20, 0, last_frame, 10, 1, 90)
bb = flag.bounding_box()
dampen_bez = Bezier()
dampen_bez.add_point(Point(0, 0), outp=Point(1/4, 0))
dampen_bez.add_point(Point(1, 1), Point(-1, 0))
damp_pc = 0.2
def dampen(p):
    relx = (p.x-bb.x1) / (bb.x2-bb.x1)
    if relx > damp_pc:
        return 1
    relx /= damp_pc
    return dampen_bez.point_at(relx).y
    #return min( / 0.2, 1)
displacer = anutils.DisplacerDampener(sine_displacer, dampen)
#displacer = sine_displacer

for g in flag.shapes[:-1]:
    for shape in g.shapes[:-2]:
        displacer.animate_bezier(shape.shape)

#displacer.amplitude *= -1
#displacer.animate_point(flag.transform.position)


script.script_main(animation, formats=["tgs", "html"])
