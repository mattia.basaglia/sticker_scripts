import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 30
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


def anim(id, angle, angle1=None):
    if angle1 is None:
        angle1 = -angle
    item = animation.find(id)
    item.transform.rotation.add_keyframe(0, angle, easing.Sigmoid())
    item.transform.rotation.add_keyframe(last_frame/2, angle1, easing.Sigmoid())
    item.transform.rotation.add_keyframe(last_frame, angle, easing.Sigmoid())


anim("leg f", 10)
anim("arm b", 6)
anim("leg b", -8)
anim("arm f", -8)
anim("head", 0, -5)


script.script_main(animation, formats=["tgs", "html"])
