import os
import math
import random
import argparse

import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie.utils.font import fonts

parser = script.get_parser(formats=["tgs", "html"])
parser.add_argument("text", nargs="?", default="AAAA")

ns = parser.parse_args()

text = ns.text
x_scale = -1
last_frame = min(40, max(180, len(text)/6*60))
animation = lottie.objects.Animation(last_frame)

src = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", "noises.svg"), 0, 1)

above = animation.add_layer(lottie.objects.ShapeLayer()).add_shape(src.find("above"))

marker = next(src.find("marker").find_all(lottie.objects.Rect)).bounding_box()
marker.x2 = 512

font = fonts["Maya"]["Regular"]
fill_color = NVector(1.0, 0.12941176470588237, 0.047058823529411764)
stroke_color = NVector(0.403921568627451, 0.050980392156862744, 0.01568627450980392)
stroke_width = 14
font_size = marker.height

textlayer = animation.add_layer(lottie.objects.ShapeLayer())
container = lottie.objects.Group()
pos = NVector(0, 0)
g = font.render(text, font_size, pos)
text_size = NVector(pos.x, g.bounding_box().height)
scale = min(marker.width / text_size.x, marker.height / text_size.y)
g.transform.scale.value *= scale
g.transform.position.value.x = g.transform.anchor_point.value.x =  text_size.x / 2
g.transform.scale.value.x *= x_scale
text_size *= scale
dy = (marker.height - text_size.y) / 2 + text_size.y
container.add_shape(g)
repeater = container.add_shape(lottie.objects.Repeater())
repeater.copies.value = 2
repeater.transform.position.value.x = text_size.x #marker.width
container.add_shape(lottie.objects.Fill(fill_color))
container.add_shape(lottie.objects.Stroke(stroke_color, stroke_width))

mask = lottie.objects.Mask()
textlayer.masks = [mask]
clip_rect = lottie.objects.Rect()
clip_rect.position.value = marker.center()
clip_rect.size.value = NVector(marker.width+stroke_width, marker.height+stroke_width)
if x_scale == -1:
    clip_rect.position.value.x = clip_rect.size.value.x / 2
mask.shape = clip_rect.to_bezier().shape

conpos = NVector(marker.x1, marker.y1 + dy)
container.transform.position.add_keyframe(0, conpos + NVector(-text_size.x, 0))
container.transform.position.add_keyframe(last_frame, conpos + NVector(0, 0))
textlayer.add_shape(container)


below = animation.add_layer(lottie.objects.ShapeLayer()).add_shape(src.find("below"))
random.seed(1)
anutils.shake([above.transform.position, below.transform.position], 8, 8, 0, last_frame, last_frame/3, easing.Jump())

if x_scale == -1:
    for layer in animation.layers:
        sh = layer.shapes.pop(0)
        g = layer.insert_shape(0, lottie.objects.Group())
        g.add_shape(sh)
        g.transform.scale.value.x *= x_scale
        g.transform.position.value = g.transform.anchor_point.value = NVector(256, 256)

script.run(animation, ns)
