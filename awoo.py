import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


head = animation.find("head")
head.transform.rotation.add_keyframe(0, -30, easing.EaseIn(0.5))
head.transform.rotation.add_keyframe(60, 0)
head.transform.rotation.add_keyframe(150, -10, easing.EaseIn(0.5))
head.transform.rotation.add_keyframe(180, -30)


for i, g in enumerate(animation.find("text").shapes[:-1]):
    start = 20 + (5-i) * 10
    end = 180

    bb = g.bounding_box().center()
    g.transform.anchor_point.value = bb
    g.transform.position.value = bb

    g.transform.opacity.add_keyframe(0, 0)
    g.transform.opacity.add_keyframe(start, 0)
    g.transform.opacity.add_keyframe(start+30, 100)
    g.transform.opacity.add_keyframe(end, 0)

    g.transform.scale.add_keyframe(0, NVector(90, 90))
    g.transform.scale.add_keyframe(start, NVector(90, 90))
    g.transform.scale.add_keyframe(start+30, NVector(100, 100))
    g.transform.scale.add_keyframe(180, NVector(140, 140))


script.script_main(animation, formats=["tgs", "html"])
