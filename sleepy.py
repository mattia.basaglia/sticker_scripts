import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


dragon = animation.find("dragon")
dragon.transform.scale.add_keyframe(0, NVector(100, 100), easing.Sigmoid())
dragon.transform.scale.add_keyframe(last_frame/2, NVector(100, 108), easing.EaseOut())
dragon.transform.scale.add_keyframe(last_frame, NVector(100, 100))
bb = dragon.bounding_box()
dragon.transform.anchor_point.value = NVector(256, bb.y2)
dragon.transform.position.value = NVector(256, bb.y2)


zlay = animation.find("Z")
#orig = zlay.bounding_box().center()
orig = zlay.transform.anchor_point.value
for g in zlay.shapes[:-1]:
    pos = g.bounding_box().center() - orig

    g.transform.opacity.add_keyframe(0, 100, easing.Sigmoid())
    g.transform.opacity.add_keyframe(last_frame/2+10, 20, easing.EaseOut())
    g.transform.opacity.add_keyframe(last_frame, 100)

    g.transform.position.add_keyframe(0, NVector(0, 0))
    g.transform.position.add_keyframe(last_frame/2+10, -pos*1.05)
    g.transform.position.add_keyframe(last_frame, NVector(0, 0))

    subg = g.shapes[0]
    subg.transform.anchor_point.value = subg.transform.position.value = subg.bounding_box().center()
    subg.transform.scale.add_keyframe(0, NVector(100, 100), easing.Sigmoid())
    subg.transform.scale.add_keyframe(last_frame/2+10, NVector(20, 20), easing.EaseOut())
    subg.transform.scale.add_keyframe(last_frame, NVector(100, 100))

    subg.transform.rotation.add_keyframe(0, 0, easing.Linear())
    subg.transform.rotation.add_keyframe(last_frame/2+10, 30, easing.EaseOut())
    subg.transform.rotation.add_keyframe(last_frame, 0)


script.script_main(animation, formats=["tgs", "html"])
