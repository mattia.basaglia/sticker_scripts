import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 120
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

animation.height = 512

layer = animation.layers[0].shapes[0]

layer.transform.anchor_point.value = NVector(512, 0) + NVector(24, -24)
layer.transform.position.value = NVector(512, 0) + NVector(-256, 256)

layer.transform.scale.value = NVector(50, 50)
#s2 = NVector(40, 40)
#s1 = NVector(47, 47)
#layer.transform.scale.add_keyframe(last_frame/8*0, s1)
#layer.transform.scale.add_keyframe(last_frame/8*1, s2)
#layer.transform.scale.add_keyframe(last_frame/8*2, s1)
#layer.transform.scale.add_keyframe(last_frame/8*3, s2)
#layer.transform.scale.add_keyframe(last_frame/8*4, s1)
#layer.transform.scale.add_keyframe(last_frame/8*5, s2)
#layer.transform.scale.add_keyframe(last_frame/8*6, s1)
#layer.transform.scale.add_keyframe(last_frame/8*7, s2)
#layer.transform.scale.add_keyframe(last_frame/8*8, s1)

#layer.transform.rotation.add_keyframe(0, +90, easing.Jump())
#layer.transform.rotation.add_keyframe(last_frame/2, 180+90)

layer.transform.rotation.add_keyframe(0, 0)
layer.transform.rotation.add_keyframe(last_frame, -360)


script.script_main(animation, formats=["tgs", "html"])
