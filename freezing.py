import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils

name = "freezing"
duration = 1
last_frame = duration*60-1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

durg = animation.find("durg")


anutils.shake(durg.transform.position, 5, 5, 0, last_frame, 40)


script.script_main(animation, formats=["tgs", "html"])
