import os
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 120
animation = objects.Animation(last_frame)

triangle_width = 512
triangle_height = math.sqrt(3)/2 * triangle_width


class Triangle:
    def __init__(self, *points):
        self.points = points
        self.midpoints = [
            (self.points[0] + self.points[1]) / 2,
            (self.points[0] + self.points[2]) / 2,
            (self.points[1] + self.points[2]) / 2
        ]

    def split(self):
        return [
            Triangle(self.points[0], self.midpoints[0], self.midpoints[1]),
            Triangle(self.points[1], self.midpoints[2], self.midpoints[0]),
            Triangle(self.points[2], self.midpoints[1], self.midpoints[2]),
        ]

    def out_shape(self, parent):
        t = parent.add_shape(objects.Path())
        t.shape.value.closed = True
        for p in self.points:
            t.shape.value.add_point(p)

    def mid_shape(self, parent):
        t = parent.add_shape(objects.Path())
        t.shape.value.closed = True
        for p in self.midpoints:
            t.shape.value.add_point(p)

    def recurse(self, steps, parent):
        self.mid_shape(parent)

        if steps == 0:
            return

        for sub in self.split():
            sub.recurse(steps-1, parent)


pt = NVector(512/2, 512-triangle_height)
pl = NVector(0, 512)
pr = NVector(512, 512)
triangle = Triangle(pt, pl, pr)


layer = objects.ShapeLayer()
animation.add_layer(layer)
g = layer.add_shape(objects.Group())
triangle.out_shape(g)
triangle.recurse(7, g)

#g.add_shape(objects.Stroke(NVector(1, 1, 0), 2))
g.add_shape(objects.Fill(NVector(1, 1, 0)))
g.transform.anchor_point.value = g.transform.position.value = pt
g.transform.scale.add_keyframe(0, NVector(400, 400))
g.transform.scale.add_keyframe(last_frame, NVector(100, 100))


script.script_main(animation, formats=["tgs", "html"])
