import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
enter_end = 40
leave_start = enter_end + 10
fade_start = leave_start + 10
leave_end = leave_start + 30
last_frame = leave_end + 30
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


animation.find("glow").transform.opacity.value = 0

telegram = animation.find("telegram")
telegram.transform.scale.add_keyframe(0, NVector(0, 0))
anutils.spring_pull(telegram.transform.scale, NVector(100, 100), 0, enter_end, 50, 7)


tgbg = animation.find("tgbg merged")
tgbg.transform.opacity.add_keyframe(fade_start, 100)
tgbg.transform.opacity.add_keyframe(leave_end, 0)


plane = animation.find("plane")
plane.transform.position.add_keyframe(leave_start, NVector(0, 0))
plane.transform.position.add_keyframe(leave_start+5, NVector(-15, 15), easing.EaseIn())
plane.transform.position.add_keyframe(leave_end, NVector(300, -300))


armf = animation.find("arm f")
pupilf = animation.find("pupilf")
pupilb = animation.find("pupilb")

nframes = 2
dt = last_frame / nframes
for i in range(3):
    armf.transform.rotation.add_keyframe(i*dt, -10, easing.EaseIn())
    armf.transform.rotation.add_keyframe(i*dt+dt/4, 5)
    armf.transform.rotation.add_keyframe(i*dt+dt/4*3, 5)

    pupilf.transform.rotation.add_keyframe(i*dt, 5, easing.EaseIn())
    pupilf.transform.rotation.add_keyframe(i*dt+dt/2, -20, easing.EaseOut())

    pupilb.transform.rotation.add_keyframe(i*dt, 15, easing.EaseIn())
    pupilb.transform.rotation.add_keyframe(i*dt+dt/2, -20, easing.EaseOut())

armf.transform.rotation.add_keyframe(last_frame, -10)
pupilf.transform.rotation.add_keyframe(last_frame, 5)
pupilb.transform.rotation.add_keyframe(last_frame, 15)



#lay = animation.insert_layer(0, lottie.objects.ShapeLayer())
#el = lay.add_shape(lottie.objects.Ellipse())
#el.size.value = NVector(20, 20)
#el.position.value = pu


script.script_main(animation, formats=["tgs", "html"])
