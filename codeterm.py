import os
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie.utils.font import FontStyle

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = objects.Animation(last_frame)


font = FontStyle("UbuntuMono", 25.5)
ex = font.ex + 3
line_height = font.line_height

code = """
Screm Dragon::rawr()
{
\tScrem rawr("Rawr");
\trawr.from = dragon;
\tif ( dragon.name == "Glax" )
\t\trawr.color = Colors::blue;
\treturn rawr;
}
""".strip()
lines = code.splitlines()
delay = 36
ch_delay = last_frame / (len(code)+delay)


def make_char(ch, parent, time, xoff, yoff):
    rendered = font.render(ch)
    group = parent.add_shape(rendered.shapes[0])
    group.transform.position.value.y += line_height * yoff
    group.transform.position.value.x += xoff

    fill = group.add_shape(objects.Fill(NVector(.8, .8, .8)))
    fill.opacity.add_keyframe(time*ch_delay, 0, easing.Jump())
    fill.opacity.add_keyframe((time+1)*ch_delay, 100)

    cursor.transform.position.add_keyframe((time+1)*ch_delay, NVector(xoff, cury), easing.Jump())
    return rendered.next_x


layer = objects.ShapeLayer()
animation.add_layer(layer)
processed = 0
g = layer.add_shape(objects.Group())
fulltext = g.add_shape(font.render(code))
fulltext.add_shape(objects.Fill(NVector(.8, .8, .8)))
fulltext.transform.position.value.y += line_height
g.transform.position.add_keyframe(0, NVector(0, 0), easing.Jump())


cursor = layer.add_shape(objects.Group())
cursize = NVector(ex, line_height)
cursor.add_shape(objects.Rect(cursize/2, cursize))
cursor.add_shape(objects.Fill(NVector(.8, .8, .8)))
cury = line_height * (1 + len(lines))
cursor.transform.position.add_keyframe(0, NVector(0, cury), easing.Jump())

for index, line in enumerate(lines):
    xoff = 0
    if index == 1:
        cury += line_height
    for i, ch in enumerate(lines[index]):
        xoff += make_char(ch, g, i+processed, xoff, index+2+len(lines))
    processed += len(lines[index])
    g.transform.position.add_keyframe(processed * ch_delay, NVector(0, -line_height*index), easing.Jump())

g.transform.position.add_keyframe((processed+delay/2) * ch_delay, NVector(0, -line_height*(index+1)), easing.Jump())


footer = layer.add_shape(objects.Group())
footer.add_shape(font.render('"/src/dragon.rawr"                  All', NVector(0, 500)))
bold = FontStyle("UbuntuMono:weight=900", 25.5)
footer.add_shape(bold.render('                    -- INSERT --       ', NVector(0, 500)))
footer.add_shape(objects.Fill(NVector(.8, .8, .8)))

animation.add_layer(objects.SolidColorLayer("#000000"))


script.script_main(animation, formats=["tgs", "html"])
