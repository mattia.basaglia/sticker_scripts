import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = "clap"
last_frame = 25
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


easeval = 0.7

hand_f = animation.find("hand f")
#hand_f.transform.skew_axis.value = -90
#hand_f.transform.skew.add_keyframe(0, 0)
#hand_f.transform.skew.add_keyframe(last_frame/2, -15)
#hand_f.transform.skew.add_keyframe(last_frame, 0)
hand_f.transform.scale.add_keyframe(0, NVector(100, 100))
hand_f.transform.scale.add_keyframe(last_frame/2, NVector(100, 90))
hand_f.transform.scale.add_keyframe(last_frame, NVector(100, 100))
#x, y = hand_f.transform.position.value
#hand_f.transform.position.add_keyframe(0, [x, y])
#hand_f.transform.position.add_keyframe(last_frame/2, [x, y+20])
#hand_f.transform.position.add_keyframe(last_frame, [x, y])

hand_f.transform.rotation.add_keyframe(0, 0, easing.EaseOut(easeval))
hand_f.transform.rotation.add_keyframe(last_frame/2, -10, easing.EaseIn(easeval))
hand_f.transform.rotation.add_keyframe(last_frame, 0)


hand_b = animation.find("hand b")
#hand_b.transform.skew_axis.value = 0
#hand_b.transform.skew.add_keyframe(0, 0)
#hand_b.transform.skew.add_keyframe(last_frame/2, 10)
#hand_b.transform.skew.add_keyframe(last_frame, 0)
hand_b.transform.scale.add_keyframe(0, NVector(100, 100))
hand_b.transform.scale.add_keyframe(last_frame/2, NVector(80, 100))
hand_b.transform.scale.add_keyframe(last_frame, NVector(100, 100))

hand_b.transform.rotation.add_keyframe(0, 0, easing.EaseOut(easeval))
hand_b.transform.rotation.add_keyframe(last_frame/2, 10, easing.EaseIn(easeval))
hand_b.transform.rotation.add_keyframe(last_frame, 0)


linelay = animation.find("lines")
for g in linelay.shapes[:-1]:
    g.transform.opacity.add_keyframe(0, 0)
    g.transform.opacity.add_keyframe(last_frame/10, 100)
    g.transform.opacity.add_keyframe(last_frame/5, 100)
    g.transform.opacity.add_keyframe(last_frame/5*3, 0)
    line = g.shapes[0]
    app = anutils.generate_path_appear(line.shape.value, 0, last_frame/5, 2, True)
    line.shape = app.shape

script.script_main(animation, formats=["tgs", "html"])
