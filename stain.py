import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]

start_crash = 30
start_jitter = 30
crash_duration = 90 - start_crash - start_jitter

crash_restore = crash_duration + start_jitter + 40

last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


objects = list(animation.find("stain").shapes[:-1])
random.shuffle(objects)


for i, o in enumerate(objects):
    pos = o.transform.position.value
    bb = o.bounding_box()
    d = start_jitter / len(objects) * i
    desty = 512 - bb.y1 + 10 + random.randint(0, 32)
    destx = random.randint(-32, 32)

    o.transform.position.add_keyframe(0, NVector(0, 0))
    o.transform.position.add_keyframe(start_crash + d, NVector(0, 0), easing.EaseIn())
    o.transform.position.add_keyframe(start_crash + d + crash_duration, NVector(destx, desty), easing.Jump())
    o.transform.position.add_keyframe(crash_restore + d + crash_duration, NVector(0, 0))


script.script_main(animation, formats=["tgs", "html"])

