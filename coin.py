import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie import objects

name = os.path.splitext(os.path.basename(__file__))[0]

rot_time = 45
last_frame = rot_time*4


coin1 = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", "coin1.svg"), 0, last_frame)
coin2 = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", "coin2.svg"), 0, last_frame)


animation = objects.Animation(last_frame)

face1 = coin1.layers[0]
animation.add_layer(face1)

face2 = coin2.layers[0]
animation.add_layer(face2)

layer = objects.ShapeLayer()
animation.add_layer(layer)


def add_shape(shape, color, parent):
    g = parent.add_shape(objects.Group())
    s = g.add_shape(shape)
    g.add_shape(objects.Fill(color))
    return s


#face1 = add_shape(objects.Ellipse(), NVector(1, 1, 0), layer)
#face2 = add_shape(objects.Ellipse(), NVector(0, 1, 0), layer)

side = layer.add_shape(objects.Group())
color = NVector(0xa1/0xff, 0x75/0xff, 0x21/0xff)
side1 = add_shape(objects.Ellipse(), color, side)
side2 = add_shape(objects.Ellipse(), color, side)
sider = add_shape(objects.Rect(), color, side)


width = 20

face1.transform.anchor_point.value = NVector(256, 256)

face1.transform.scale.add_keyframe(rot_time*0, NVector(100, 100), easing.EaseIn())
face1.transform.scale.add_keyframe(rot_time*1, NVector(0, 100))
face1.transform.scale.add_keyframe(rot_time*2, NVector(0, 100))
face1.transform.scale.add_keyframe(rot_time*3, NVector(0, 100), easing.EaseOut())
face1.transform.scale.add_keyframe(rot_time*4, NVector(100, 100))

face1.transform.position.add_keyframe(rot_time*0, NVector(256, 256), easing.EaseIn())
face1.transform.position.add_keyframe(rot_time*1, NVector(256-width, 256))
face1.transform.position.add_keyframe(rot_time*2, NVector(256-width, 256))
face1.transform.position.add_keyframe(rot_time*3, NVector(256+width, 256), easing.EaseOut())
face1.transform.position.add_keyframe(rot_time*4, NVector(256, 256))


face2.transform.anchor_point.value = NVector(256, 256)

face2.transform.scale.add_keyframe(rot_time*0, NVector(0, 100))
face2.transform.scale.add_keyframe(rot_time*1, NVector(0, 100), easing.EaseOut())
face2.transform.scale.add_keyframe(rot_time*2, NVector(100, 100), easing.EaseIn())
face2.transform.scale.add_keyframe(rot_time*3, NVector(0, 100))

face2.transform.position.add_keyframe(rot_time*0, NVector(256+width, 256))
face2.transform.position.add_keyframe(rot_time*1, NVector(256+width, 256), easing.EaseOut())
face2.transform.position.add_keyframe(rot_time*2, NVector(256, 256), easing.EaseIn())
face2.transform.position.add_keyframe(rot_time*3, NVector(256-width, 256))


side1.size.add_keyframe(rot_time*0, NVector(512, 512), easing.EaseIn())
side1.size.add_keyframe(rot_time*1, NVector(0, 512))
side1.size.add_keyframe(rot_time*2, NVector(0, 512))
side1.size.add_keyframe(rot_time*3, NVector(0, 512), easing.EaseOut())
side1.size.add_keyframe(rot_time*4, NVector(512, 512))

side1.position.add_keyframe(rot_time*0, NVector(256, 256), easing.EaseIn())
side1.position.add_keyframe(rot_time*1, NVector(256+width, 256))
side1.position.add_keyframe(rot_time*2, NVector(256-width, 256))
side1.position.add_keyframe(rot_time*3, NVector(256-width, 256), easing.EaseOut())
side1.position.add_keyframe(rot_time*4, NVector(256, 256))


sider.position.value = NVector(256, 256)
sider.size.add_keyframe(rot_time*0, NVector(0, 511))
sider.size.add_keyframe(rot_time*1, NVector(2*width, 511))
sider.size.add_keyframe(rot_time*2, NVector(0, 511))
sider.size.add_keyframe(rot_time*3, NVector(2*width, 511))


side2.size.add_keyframe(rot_time*0, NVector(0, 512))
side2.size.add_keyframe(rot_time*1, NVector(0, 512), easing.EaseOut())
side2.size.add_keyframe(rot_time*2, NVector(512, 512), easing.EaseIn())
side2.size.add_keyframe(rot_time*3, NVector(0, 512))

side2.position.add_keyframe(rot_time*0, NVector(256-width, 256))
side2.position.add_keyframe(rot_time*1, NVector(256-width, 256), easing.EaseOut())
side2.position.add_keyframe(rot_time*2, NVector(256, 256), easing.EaseIn())
side2.position.add_keyframe(rot_time*3, NVector(256+width, 256))
side2.position.add_keyframe(rot_time*4, NVector(256-width, 256))


script.script_main(animation, formats=["tgs", "html"])
