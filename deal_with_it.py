import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]

arrive_end = 80
fade_start = arrive_end + 80
last_frame = fade_start + 20
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", "unimpressed.svg"), 0, last_frame)

#t = animation.layers[0].transform
#t.position.value = t.anchor_point.value = NVector(256, 0)
#t.scale.value = NVector(-100, 100)

animation.find("eye f").transform.opacity.value = 0
animation.find("eye b").transform.opacity.value = 0
animation.find("dealeyes").transform.opacity.value = 100
glaxes = animation.find("glaxes")
glaxes.transform.position.add_keyframe(0, NVector(0, -300))
glaxes.transform.position.add_keyframe(arrive_end, NVector(0, 0))
glaxes.transform.opacity.add_keyframe(fade_start, 100)
glaxes.transform.opacity.add_keyframe(last_frame, 0)

script.script_main(animation, formats=["tgs", "html"])
