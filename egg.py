import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils

name = "egg"
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


egg = animation.find("egg")
ang = 5
anutils.rot_shake(egg.transform.rotation, [-ang, ang], 0, 60, 16)


script.script_main(animation, formats=["tgs", "html"])
