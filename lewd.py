import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
import random

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 140
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


lewd = animation.find("lewd")
lewd.transform.rotation.value = 30
anutils.spring_pull(lewd.transform.rotation, 0, 0, 60)
lewd.transform.rotation.add_keyframe(80, 0, easing.EaseIn(0.8))
lewd.transform.rotation.add_keyframe(100, 30)


script.script_main(animation, formats=["tgs", "html"])
