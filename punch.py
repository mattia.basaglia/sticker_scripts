import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie import NVector

name = "punch"
duration = 1
last_frame = duration*60-1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

fist = animation.find("fist")

fist.transform.anchor_point.value = fist.transform.position.value = NVector(256, 256)
fist.transform.scale.add_keyframe(0, NVector(60, 60))
fist.transform.scale.add_keyframe(30, NVector(30, 30))
fist.transform.scale.add_keyframe(40, NVector(120, 120))
fist.transform.scale.add_keyframe(60, NVector(60, 60))

script.script_main(animation, formats=["tgs", "html"])
