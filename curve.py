import os
import sys
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector, Color
from lottie.utils.font import FontShape, FontStyle

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = objects.Animation(last_frame)
import os
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie.utils.color import from_uint8

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = objects.Animation(last_frame)
n_steps = 6
step = last_frame / (n_steps+2)


p0_l = NVector(512*2/8, 512/3)
p0_r = NVector(512*7/8, 512/3)


def next_point(l, r, dirx, diry):
    m = (l+r) / 2
    t = m - l
    return m + NVector(dirx * t.y, diry * t.x)



precomp_0 = objects.Precomp("precomp_0", animation)
animation.assets.append(precomp_0)

layer = objects.ShapeLayer()
#layer.in_point = 0
#layer.out_point = step
precomp_0.add_layer(layer)
path = layer.add_shape(objects.Path())

easingfn = easing.Sigmoid(1/2)

bez0 = objects.Bezier()
bez0.add_point(p0_l)
bez0.add_point(p0_l.lerp(p0_r, 1/4))
bez0.add_point((p0_l+p0_r)/2)
bez0.add_point(p0_l.lerp(p0_r, 3/4))
bez0.add_point(p0_r)
path.shape.add_keyframe(0, bez0, easingfn)

p1 = next_point(p0_l, p0_r, 1, 1)
bez1 = objects.Bezier()
bez1.add_point(p0_l)
bez1.add_point((p0_l+p1)/2)
bez1.add_point(p1)
bez1.add_point((p0_r+p1)/2)
bez1.add_point(p0_r)
path.shape.add_keyframe(step/2, bez1, easingfn)

p2_l = next_point(p0_l, p1, -1, 1)
p2_r = next_point(p1, p0_r, 1, -1)
bez2 = objects.Bezier()
bez2.add_point(p0_l)
bez2.add_point(p2_l)
bez2.add_point(p1)
bez2.add_point(p2_r)
bez2.add_point(p0_r)
path.shape.add_keyframe(step, bez2)

s1 = layer.add_shape(objects.Stroke(from_uint8(50, 80, 176), 20))
s1.width.add_keyframe(0, 40, easingfn)
s1.width.add_keyframe(step/2, 30, easingfn)
s1.width.add_keyframe(step, 20)

s2 = layer.add_shape(objects.Stroke(from_uint8(29, 40, 72), 26))
s2.width.add_keyframe(0, 52, easingfn)
s2.width.add_keyframe(step/2, 39, easingfn)
s2.width.add_keyframe(step, 26)

#s1.opacity = s2.opacity
#s1.opacity.add_keyframe(step, 100, easing.Jump())
#s1.opacity.add_keyframe(step+1, 0)


def substep(prev, pl):
    subl = objects.PreCompLayer(prev)
    subl.start_time = step
    subl.transform.rotation.value = 90
    subl.transform.scale.value = NVector(100, 100) / (math.sqrt(2)**2)
    subl.transform.position.value = subl.transform.anchor_point.value = pl
    return subl


def make_step(index, pl, pr):
    prev = "precomp_%s" % (index-1)
    cur = "precomp_%s" % (index)
    precomp = objects.Precomp(cur, animation)
    animation.assets.append(precomp)

    arm_ll = substep(prev, pl)
    precomp.add_layer(arm_ll)

    arm_lr = substep(prev, pl)
    arm_lr_t = objects.NullLayer()
    arm_lr.parent_index = arm_lr_t.index = 100 + index
    arm_lr_t.transform.position.value = arm_lr_t.transform.anchor_point.value = p2_l
    arm_lr_t.transform.rotation.value = 90
    precomp.add_layer(arm_lr)
    precomp.add_layer(arm_lr_t)

    arm_rl = substep(prev, pl)
    arm_rl_t = objects.NullLayer()
    arm_rl.parent_index = arm_rl_t.index = 110 + index
    arm_rl_t.transform.position.value = arm_rl_t.transform.anchor_point.value = (p2_l+p2_r)/2
    arm_rl_t.transform.rotation.value = 180
    precomp.add_layer(arm_rl)
    precomp.add_layer(arm_rl_t)

    arm_rr = substep(prev, pl)
    arm_rr_t = objects.NullLayer()
    arm_rr.parent_index = arm_rr_t.index = 120 + index
    arm_rr_t.transform.position.value = arm_rr_t.transform.anchor_point.value = p1
    arm_rr_t.transform.rotation.value = 90
    precomp.add_layer(arm_rr)
    precomp.add_layer(arm_rr_t)


last_layer = animation.add_layer(objects.PreCompLayer("precomp_0"))
for i in range(1, n_steps):
    last_layer.out_point = step * i
    make_step(i, p0_l, p0_r)
    last_layer = animation.add_layer(objects.PreCompLayer("precomp_%s" % i))
    sys.stderr.write("%s %s\n" % (i, step * (i+1)))


script.script_main(animation, formats=["tgs", "html"])
