import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils

name = "cyborg"
last_frame = 180 # max
batt_delay = last_frame / 5 / 2
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


battery1 = animation.find("battery 1")
battery2 = animation.find("battery 2")
battery3 = animation.find("battery 3")
batt_time = 0
for i in range(2):
    battery1.transform.opacity.add_keyframe(batt_time, 0)
    battery2.transform.opacity.add_keyframe(batt_time, 0)
    battery3.transform.opacity.add_keyframe(batt_time, 0)

    batt_time += batt_delay
    battery1.transform.opacity.add_keyframe(batt_time, 100)
    battery2.transform.opacity.add_keyframe(batt_time, 0)

    batt_time += batt_delay
    battery2.transform.opacity.add_keyframe(batt_time, 100)
    battery3.transform.opacity.add_keyframe(batt_time, 0)

    batt_time += batt_delay
    battery1.transform.opacity.add_keyframe(batt_time, 100)
    battery2.transform.opacity.add_keyframe(batt_time, 100)
    battery3.transform.opacity.add_keyframe(batt_time, 100)

    batt_time += batt_delay
    battery1.transform.opacity.add_keyframe(batt_time, 0)
    battery2.transform.opacity.add_keyframe(batt_time, 0)
    battery3.transform.opacity.add_keyframe(batt_time, 0)
    batt_time += batt_delay


battery_out = animation.find("battery")
blinks = 4
hb = last_frame / blinks / 2
for i in range(blinks):
    battery_out.transform.opacity.add_keyframe((2 * i) * hb, 100)
    battery_out.transform.opacity.add_keyframe((2 * i + 1) * hb, 20)
battery_out.transform.opacity.add_keyframe(last_frame, 100)


dragon = animation.find("dragon")
dragon.transform.rotation.add_keyframe(0, -1)
dragon.transform.rotation.add_keyframe(last_frame/2, 2)
dragon.transform.rotation.add_keyframe(last_frame, -1)


dragon = animation.find("head")
dragon.transform.rotation.add_keyframe(0, 2)
dragon.transform.rotation.add_keyframe(last_frame/2, -1)
dragon.transform.rotation.add_keyframe(last_frame, 2)

eyes = animation.find("eyes")
eyes.transform.rotation.add_keyframe(0, 0)
eyes.transform.rotation.add_keyframe(last_frame/5*1, 0)
eyes.transform.rotation.add_keyframe(last_frame/5*2, -5)
eyes.transform.rotation.add_keyframe(last_frame/5*4, -5)
eyes.transform.rotation.add_keyframe(last_frame, 0)

script.script_main(animation, formats=["tgs", "html"])
