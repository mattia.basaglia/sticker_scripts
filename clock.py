import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script

name = "clock"
duration = 3
last_frame = duration*60-1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

hand_hours = animation.find("hand_hours")
hand_minutes = animation.find("hand_minutes")


hand_minutes.transform.rotation.add_keyframe(0, 60)
hand_minutes.transform.rotation.add_keyframe(30, 60+180)
hand_minutes.transform.rotation.add_keyframe(60, 60+360)
hand_minutes.transform.rotation.add_keyframe(90, 60+360+180)
hand_minutes.transform.rotation.add_keyframe(120, 60+360+360)
hand_minutes.transform.rotation.add_keyframe(150, 60+360*2+180)
hand_minutes.transform.rotation.add_keyframe(last_frame, 60+360*3)


hand_hours.transform.position.value = hand_minutes.transform.position.value
hand_hours.transform.rotation.add_keyframe(0, -54)
hand_hours.transform.rotation.add_keyframe(last_frame/2, -54-180)
hand_hours.transform.rotation.add_keyframe(last_frame, -54-360)

script.script_main(animation, formats=["tgs", "html"])
