import os
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie import NVector

name = "swearing"
duration = 1
last_frame = duration*60-1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

durg = animation.find("durg")
swear = animation.find("swear")


anutils.shake(durg.transform.position, 10, 15, 0, last_frame, 25)


swear.transform.anchor_point.value = swear.transform.position.value = NVector(373, 396)


def random_rot_shake(rotation_prop, min_angle, max_angle, start_time, end_time, n_frames):
    frame_time = (end_time - start_time) / n_frames
    if not rotation_prop.animated:
        start = rotation_prop.value
    else:
        start = rotation_prop.keyframes[-1].start
    delta = max_angle - min_angle

    first = 0
    rotation_prop.add_keyframe(start_time, start)
    for i in range(1, n_frames):
        a = start + min_angle + (random.random() * delta)
        rotation_prop.add_keyframe(start_time + i * frame_time, a)
    rotation_prop.add_keyframe(end_time, start)


random_rot_shake(swear.transform.rotation, -25, 15, 0, 60, 15)
anutils.shake(swear.transform.position, 10/3, 15/3, 0, last_frame, 25)


script.script_main(animation, formats=["tgs", "html"])
