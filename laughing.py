import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

animation.find("paths").transform.opacity.value = 0

face = animation.find("face")
ease = easing.Sigmoid()
shake_speed = 10
for i in range(0, last_frame, shake_speed*2):
    face.transform.rotation.add_keyframe(i, -2, ease)
    face.transform.rotation.add_keyframe(i+shake_speed, 2, ease)

face.transform.rotation.add_keyframe(last_frame, -2, ease)


def get_paths(layer):
    return animation.find(layer).find_all(lottie.objects.Group, None, False)


droplet_start = 30
droplet_time = 60
n_keyframes = 10
for i, (droplet, path) in enumerate(zip(get_paths("droplets"), get_paths("paths"))):
    bez = path.shapes[0].shape.value
    tr = droplet.transform
    start = droplet_start * i
    duration = droplet_time
    anutils.follow_path(
        tr.position, bez,
        start, start + duration, n_keyframes,
        False, NVector(0, 0), 0,
        tr.rotation, 0
    )
    tr.scale.add_keyframe(start + duration * 0.7, NVector(100, 100))
    tr.scale.add_keyframe(start + duration, NVector(0, 0))


script.script_main(animation, formats=["tgs", "html"])

