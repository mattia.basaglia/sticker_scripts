import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


def dampen(p):
    f = p.x / 512
    return 0.5 - math.cos(f * math.pi) / 2


displacer = anutils.SineDisplacer(400, 32, 0, last_frame, 8, 1, 90)
#displacer = anutils.DisplacerDampener(displacer, dampen)

for b in animation.find("tail").find_all(lottie.objects.Path):
    b.shape.value.split_each_segment()
    b.shape.value.split_each_segment()
    displacer.animate_bezier(b.shape)

displacer.animate_point(animation.find("butt").transform.position)

lamax = 35
lamin = -lamax

leg = animation.find("leg")
leg.transform.rotation.add_keyframe(0, 0)
leg.transform.rotation.add_keyframe(last_frame/4, lamin)
leg.transform.rotation.add_keyframe(last_frame*3/4, lamax)
leg.transform.rotation.add_keyframe(last_frame, 0)


leg1 = animation.find("leg 1")
leg1.transform.rotation.add_keyframe(0, 0)
leg1.transform.rotation.add_keyframe(last_frame/4, -lamin)
leg1.transform.rotation.add_keyframe(last_frame*3/4, -lamax)
leg1.transform.rotation.add_keyframe(last_frame, 0)


t = animation.layers[0].transform
sp = NVector(0, 512)
t.anchor_point.value = t.position.value = sp
t.scale.value = NVector(90, 90)
leglen = 45
d = math.cos((lamin+25)/180*math.pi) * leglen
t.position.add_keyframe(0, sp)
t.position.add_keyframe(last_frame/4, sp + NVector(-d, 0))
t.position.add_keyframe(last_frame*3/4, sp + NVector(d, 0))
t.position.add_keyframe(last_frame, sp)


script.script_main(animation, formats=["tgs", "html"])
