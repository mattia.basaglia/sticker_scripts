import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie import NVector

name = "run-away"
last_frame = 80
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

durg = animation.find("durg")

#durg.transform.position.value = NVector(300, 300)
durg.transform.rotation.add_keyframe(0, 10)
durg.transform.rotation.add_keyframe(50, 0)
durg.transform.rotation.add_keyframe(60, -10)
durg.transform.position.add_keyframe(0, NVector(880, 300))
durg.transform.position.add_keyframe(50, NVector(500, 300))
durg.transform.position.add_keyframe(last_frame, NVector(-380, 300))

script.script_main(animation, formats=["tgs", "html"])
