import os
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 20
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


def get_marker(name):
    return next(animation.find(name).find_all(objects.Ellipse)).position.value


animation.find("markers").transform.opacity.value = 0

marker_jaw = get_marker("marker_jaw")
marker_arm = get_marker("marker_arm")
marker_front = get_marker("marker_front")

derg_f = animation.find("derg f")

jaw_f = animation.find("jaw f")
jaw_b = animation.find("jaw b")

paw = animation.find("paw")
paw_f = animation.find("paw f")

jaw_angle = 2
head_angle = 5
arm_angle = 5
ease = easing.Sigmoid()


#def dist_depth_group(group, multiplier, offset, pos):
    #if isinstance(group, lottie.objects.Group):
        #for c in group.shapes[:-1]:
            #dist_depth_group(c, multiplier, offset, pos)
    #elif isinstance(group, lottie.objects.Path):
        #bez = group.shape.value
        #for i in range(len(bez.vertices)):
            #x = bez.vertices[i]
            #depth = (x - pos).length * multiplier / 512 + offset
            #bez.vertices[i].components.append(depth)
            #din = (x + bez.in_tangents[i] - pos).length * multiplier / 512 + offset - depth
            #bez.in_tangents[i].components.append(din)
            #dout = (x + bez.out_tangents[i] - pos).length * multiplier / 512 + offset - depth
            #bez.out_tangents[i].components.append(dout)

        #forward.animate_bezier(group.shape)
        #backward.animate_bezier(group.shape)


#def dist_depth_layer(name, multiplier, offset, pos):
    #for g in animation.find(name).shapes[:-1]:
        #dist_depth_group(g, multiplier, offset, pos)


#axis = NVector(4, -1, -2)
#rot_c = marker_jaw.clone()
#depth = 34

#forward = anutils.DepthRotationDisplacer(
    #rot_c,
    #0, last_frame/2, 10,
    #axis,
    #depth, -0, 0,
    #ease
#)
#backward = anutils.DepthRotationDisplacer(
    #rot_c,
    #last_frame/2, last_frame, 10,
    #axis,
    #depth, 0, 0,
    #ease
#)

#forward.angle_start = -head_angle
#forward.angle = head_angle
#backward.angle_start = head_angle
#backward.angle = -head_angle
#dist_depth_layer("derg f", 100, 0, marker_front)


#forward.angle_start = jaw_angle
#forward.angle = -jaw_angle
#backward.angle_start = -jaw_angle
#backward.angle = jaw_angle
#dist_depth_layer("jaw f", 100, 0, marker_front)
#dist_depth_layer("jaw b", 100, 0, marker_front)

derg_f.transform.anchor_point.value = derg_f.transform.position.value = marker_jaw

derg_f.transform.rotation.add_keyframe(0, head_angle, ease)
derg_f.transform.rotation.add_keyframe(last_frame/2, 0, ease)
derg_f.transform.rotation.add_keyframe(last_frame, head_angle, ease)

jaw_f.transform.anchor_point.value = jaw_f.transform.position.value = marker_jaw
jaw_b.transform.anchor_point.value = jaw_b.transform.position.value = marker_jaw

jaw_f.transform.rotation.add_keyframe(0, -jaw_angle, ease)
jaw_f.transform.rotation.add_keyframe(last_frame/2, 0, ease)
jaw_f.transform.rotation.add_keyframe(last_frame, -jaw_angle, ease)

jaw_b.transform.rotation.add_keyframe(0, -jaw_angle, ease)
jaw_b.transform.rotation.add_keyframe(last_frame/2, 0, ease)
jaw_b.transform.rotation.add_keyframe(last_frame, -jaw_angle, ease)


paw.transform.anchor_point.value = paw.transform.position.value = marker_arm
paw_f.transform.anchor_point.value = paw_f.transform.position.value = marker_arm

paw.transform.rotation.add_keyframe(0, -arm_angle, ease)
paw.transform.rotation.add_keyframe(last_frame/2, 0, ease)
paw.transform.rotation.add_keyframe(last_frame, -arm_angle, ease)

paw_f.transform.rotation.add_keyframe(0, -arm_angle, ease)
paw_f.transform.rotation.add_keyframe(last_frame/2, 0, ease)
paw_f.transform.rotation.add_keyframe(last_frame, -arm_angle, ease)


script.script_main(animation, formats=["tgs", "html"])
