import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

animation.layers[0].transform.position.value = NVector(24, 0)


halo = animation.find("halo")
halob = animation.find("halo b")
random.seed(0)
sparkles = [
    (x.shapes[0], x.shapes[0].size.value, random.random() * math.pi * 2)
    for x in animation.find("sparkles").shapes[:-1]
]

n_frames = 16
lengthx = 64
lengthy = 16

for i in range(n_frames):
    ratio = i / (n_frames-1)
    t = last_frame * ratio
    a = 2 * math.pi * ratio
    x = lengthx * math.cos(a)
    y = lengthy * math.sin(a)
    pos = NVector(x, y)
    halo.transform.position.add_keyframe(t, pos)
    halob.transform.position.add_keyframe(t, pos)

    for sparkle, startsize, sa in sparkles:
        r = startsize.y / 2
        size = NVector(startsize.x, r + r * (math.cos(a+sa) / 2 + 0.5))
        sparkle.size.add_keyframe(t, size)


script.script_main(animation, formats=["tgs", "html"])
