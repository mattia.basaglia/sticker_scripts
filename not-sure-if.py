import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
zoom_in_end = 110
zoom_out_start = zoom_in_end + 30
zoom_out_end = 160
last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


def animate(prop, s, e):
    prop.add_keyframe(0, s, easing.Sigmoid(0.8))
    prop.add_keyframe(zoom_in_end, e)
    prop.add_keyframe(zoom_out_start, e, easing.Sigmoid())
    prop.add_keyframe(zoom_out_end, s)


layer = animation.layers[0]
layer.transform.position.value = layer.transform.anchor_point.value = NVector(292, 80)
animate(layer.transform.scale, NVector(100, 100), NVector(130, 130))

# Left
eyelid_tl = animation.find("eyelid_tl")
animate(eyelid_tl.transform.position, NVector(0, -5), NVector(0, 0))
eyelid_tl_f = animation.find("eyelid_tl_f")
animate(eyelid_tl_f.transform.position, NVector(-2, -5), NVector(0, 0))

eyelid_bl = animation.find("eyelid_bl")
animate(eyelid_bl.transform.position, NVector(0, 3), NVector(0, 0))
eyelid_bl_f = animation.find("eyelid_bl_f")
animate(eyelid_bl_f.transform.position, NVector(0, 3), NVector(0, 0))

pupil_l = animation.find("pupil_l")
pupil_l.transform.position.value = pupil_l.transform.anchor_point.value = pupil_l.bounding_box().center()
animate(pupil_l.transform.scale, NVector(150, 150), NVector(100, 100))

# Right
eyelid_tr = animation.find("eyelid_tr")
animate(eyelid_tr.transform.position, NVector(-2, -4), NVector(0, 0))
eyelid_tr_f = animation.find("eyelid_tr_f")
animate(eyelid_tr_f.transform.position, NVector(-2, -4), NVector(0, 0))

eyelid_br = animation.find("eyelid_br")
animate(eyelid_br.transform.position, NVector(-2, 3), NVector(0, 0))
eyelid_br_f = animation.find("eyelid_br_f")
animate(eyelid_br_f.transform.position, NVector(-1, 3), NVector(0, 0))

pupil_r = animation.find("pupil_r")
pupil_r.transform.position.value = pupil_r.transform.anchor_point.value = pupil_r.bounding_box().center()
animate(pupil_r.transform.scale, NVector(150, 150), NVector(100, 100))


script.script_main(animation, formats=["tgs", "html"])
