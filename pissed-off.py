import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector, objects, Color, PolarVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


markers = animation.find("markers")
markers.transform.opacity.value = 0
smoker = markers.find("smoker").shapes[0]

layer = animation.insert_layer(0, objects.ShapeLayer())
color1 = Color(0.1, 0, 0)
color2 = Color(0.4, 0.4, 0.4)
color3 = Color(1, 1, 1)
particle_start = smoker.position.value
opacity_start = 100
opacity_end = 20
end_len = 128
particle_size = NVector(20, 20)
start_random = 20


def fx(t):
    return math.cos(t**0.2*math.pi*8) * fy(t)**2


def fy(t):
    return math.sin(t**0.3*math.pi/2)


def particle(t):
    g = layer.add_shape(objects.Group())
    b = g.add_shape(objects.Ellipse())
    b.size.value = particle_size
    fill = objects.Fill()
    g.add_shape(fill)

    if t < 1/2:
        lf = t * 2
        fill.color.add_keyframe(0, color1.lerp(color2, lf))
        fill.color.add_keyframe(last_frame*(.5 - t), color2)
        fill.color.add_keyframe(last_frame*(1 - t)-1, color3, easing.Jump())
        fill.color.add_keyframe(last_frame*(1 - t), color1)
        fill.color.add_keyframe(last_frame, color1.lerp(color2, lf))
    else:
        lf = (t-0.5) * 2
        tf = (1-lf)/2
        fill.color.add_keyframe(0, color2.lerp(color3, lf))
        fill.color.add_keyframe(last_frame*tf-1, color3, easing.Jump())
        fill.color.add_keyframe(last_frame*tf, color1)
        fill.color.add_keyframe((.5 + tf) * last_frame, color2)
        fill.color.add_keyframe(last_frame, color2.lerp(color3, lf))

    fill.opacity.add_keyframe(0, opacity_start + (opacity_end - opacity_start) * t)
    fill.opacity.add_keyframe((1 - t) * last_frame, opacity_end)
    fill.opacity.add_keyframe((1 - t) * last_frame+1, opacity_start)
    fill.opacity.add_keyframe(last_frame, opacity_start + (opacity_end - opacity_start) * t)

    scalex = random.random() * end_len * 2 - end_len
    #scalex = 0
    startpos = particle_start + PolarVector(random.random() * start_random, random.random() * math.pi * 2)
    scaley = startpos.y + particle_size.y
    n_frames = 36
    oldk = 0
    for i in range(n_frames):
        f = i / (n_frames-1)
        k = (f + t)
        if k > 1:
            k -= 1
        ft = (k) ** 4
        foo = False
        pos = startpos + NVector(scalex*fx(ft), -scaley * fy(ft))
        if oldk > k:
            easing.Jump()(b.position.keyframes[-1])
            #pos.y = startpos.y
            #ft = 0
            #k = 0
        oldk = k
        b.position.add_keyframe(last_frame*f, pos)


random.seed(1)
for i in range(100):
    #t = random.random()
    t = i/99
    particle(t)

script.script_main(animation, formats=["tgs", "html"], strip=lottie.utils.stripper.heavy_strip)
