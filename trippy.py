import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie import objects
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector, Point
from lottie import Color
from lottie.nvector import PolarVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", "blep.svg"), 0, last_frame)

animation.layers[0].name = "svg"


layer = animation.find("durg")
layer.transform.anchor_point.value = Point(256, 256)
layer.transform.position.value = Point(256, 256)
layer.transform.rotation.add_keyframe(0, 0)
layer.transform.rotation.add_keyframe(last_frame, 360 * 2)


n_frames = 24
for fill in layer.find_all((objects.Fill, objects.Stroke)):
    color = fill.color.value
    color.convert(Color.Mode.LCH_uv)

    for frame in range(n_frames):
        off = frame / (n_frames-1)
        color.hue = (color.hue + 3 * math.tau / (n_frames-1)) % math.tau
        fill.color.add_keyframe(off * last_frame, color.to_rgb())


#layer.transform.opacity.value = 0

spiral = animation.add_layer(objects.ShapeLayer()).add_shape(objects.Group())


circle_r = 17
circle_count = 150
angle = 137.5
steps = 13
distance = 45
center = NVector(256, 256)


ball_color = Color(1, 0, 0).convert(Color.Mode.LCH_uv)
for i in range(-2, circle_count):
    g = spiral.add_shape(objects.Group())
    sh = g.add_shape(objects.Ellipse())
    sh.size.value = NVector(circle_r*2, circle_r*2)
    sh.position.add_keyframe(0, center + NVector(math.sqrt(max(0, i)) * distance, 0))
    sh.position.add_keyframe(last_frame, center + NVector(math.sqrt(i + steps) * distance, 0))

    g.transform.rotation.add_keyframe(0, angle * i)
    g.transform.rotation.add_keyframe(last_frame, angle * (i+steps))
    g.transform.anchor_point.value = center
    g.transform.position.value = center

    fill = g.add_shape(objects.Fill())
    color = ball_color.clone()
    h_start = (angle * math.pi / 180 * i) % math.tau
    h_end = (angle * math.pi / 180 * (i + steps)) % math.tau
    if h_end - h_start < math.pi:
        h_end += math.tau


    for frame in range(n_frames):
        off = frame / (n_frames-1)
        color.h = h_start * (1-off) + h_end * off
        fill.color.add_keyframe(off * last_frame, color.to_rgb())


spiral.transform.rotation.add_keyframe(0, 0)
spiral.transform.rotation.add_keyframe(last_frame, -360*5)
spiral.transform.position.value = spiral.transform.anchor_point.value = center

script.script_main(animation, formats=["tgs", "html"])
