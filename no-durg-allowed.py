import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
enter_end = 60
bg_enter_start = 15
bg_enter_end = bg_enter_start + enter_end
last_frame = enter_end + 40
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

size = 70

nots = animation.find("not")
nots.transform.anchor_point.value = nots.transform.position.value = nots.bounding_box().center()
nots.transform.scale.add_keyframe(0, NVector(200, 200), easing.Sigmoid(0.6))
nots.transform.scale.add_keyframe(enter_end, NVector(size, size))
nots.transform.opacity.add_keyframe(0, 0, easing.Sigmoid(0.6))
nots.transform.opacity.add_keyframe(enter_end, 100)

sign = animation.find("sign")
sign.transform.anchor_point.value = sign.transform.position.value = sign.bounding_box().center()
sign.transform.scale.add_keyframe(0, NVector(0, 0))
anutils.spring_pull(sign.transform.scale, NVector(size, size), bg_enter_start, bg_enter_end)

durg = animation.find("durg")
durg.transform.anchor_point.value = durg.transform.position.value = durg.bounding_box().center()
durg.transform.scale.add_keyframe(bg_enter_start, NVector(100, 100), easing.Sigmoid(0.6))
durg.transform.scale.add_keyframe(bg_enter_end, NVector(size, size))

script.script_main(animation, formats=["tgs", "html"])
