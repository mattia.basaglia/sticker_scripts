#!/usr/bin/env python3
import os
import sys
import subprocess
import lottie

path = os.path.dirname(__file__)
for file in os.listdir(path):
    if file.endswith(".py") and not file.startswith("_"):
        full = os.path.join(path, file)
        cmd = ["python3", full] + sys.argv[1:]
        print(" ".join(cmd))
        subprocess.call(cmd)
