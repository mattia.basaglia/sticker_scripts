import os
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector, Color
from lottie.utils.font import FontShape, FontStyle
from lottie.utils.color import Color, ColorMode

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 180
#animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)
animation = objects.Animation(last_frame)
offset_time = 5

font = FontStyle("UbuntuMono", 25.5)
ex = font.ex + 3
line_height = font.line_height
loop_time = 120
n_lines = 0


parser = script.get_parser(formats=["tgs", "html"])
parser.add_argument(
    "--columns",
    nargs="+",
    default=[
        "".join(["glax", "derg", "glax", "rawr", "derg", "rawr"]),
        "".join(["dragons!", "rawr", "dragons!", "glax"]),
        "".join(["derg", "rawr", "glax", "dragons!", "glax"]),
        "".join(["rawr", "glax", "dragons!", "glax", "derg"]),
        "".join(["glax", "dragons!", "glax", "rawr", "rawr"]),
    ]
)
parser.add_argument(
    "--n-rows",
    type=int,
    default=6*4
)
ns = parser.parse_args()


def character(ch, parent, time, yoff):
    group = parent.add_shape(font.render(ch).shapes[0])
    group.transform.position.value.y += line_height * yoff
    fill = group.add_shape(objects.Fill())
    #color = Color(yoff / (4*6), 1, 1, ColorMode.HSV).to_color()
    color = Color(0, 1, 0)
    fill.color.add_keyframe(time+0, color)
    fill.color.add_keyframe(time+40, Color(0, 0, 0), easing.Jump())

    if time != 0:
        fill.opacity.add_keyframe(0, 0, easing.Jump())
        fill.opacity.add_keyframe(time, 100, easing.Jump())
    fill.opacity.add_keyframe(time+20, 100)
    fill.opacity.add_keyframe(time+80, 0)
    group.add_shape(objects.Stroke(Color(0, 0, 0), 2)).opacity = fill.opacity


def add_rain(id, x, y, parent):
    layer = objects.PreCompLayer(id)
    parent.add_layer(layer)
    layer.transform.position.value.x = ex * x
    layer.transform.position.value.y = line_height * y * 4
    start_time = y * offset_time * 4

    layer.start_time = start_time


def make_line(str):
    global n_lines
    id = "line%s" % n_lines
    n_lines += 1
    pc = objects.Precomp(id, animation)
    animation.assets.append(pc)

    layer = pc.add_layer(objects.ShapeLayer())
    for i, c in enumerate(str):
        character(c.upper(), layer, i * offset_time, i+1)


def add_line(id, x, off):
    pcl = objects.PreCompLayer(id)
    animation.add_layer(pcl)
    pcl.transform.position.value.x = x * ex
    start_time = last_frame * off
    pcl.start_time = start_time


for column in ns.columns:
    line = column

    while len(line) < ns.n_rows:
        line += column

    make_line(line[:ns.n_rows])

n_cols = 32
n_offsets = 16
off = []
for i in range(0, n_cols, n_offsets):
    toff = list(range(n_offsets))
    random.shuffle(toff)

    while off and off[-1] in toff[:3]:
        random.shuffle(toff)

    off += toff

for i in range(n_cols):
    normoff = off[i] / n_offsets
    lineid = "line%s" % random.randint(0, n_lines-1)
    #print("add_line(%r, %s, %s)" % (lineid, i, normoff))
    #print("add_line(%r, %s, %s)" % (lineid, i, normoff-1))
    add_line(lineid, i, normoff)
    add_line(lineid, i, normoff-1)


script.float_strip(animation)
script.run(animation, ns)

