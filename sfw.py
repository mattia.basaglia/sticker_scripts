import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]

write_start = 20
write_end = write_start + 60
return_end = write_end + 30
fade_start = return_end
fade_end = return_end + 60
last_frame = fade_end
print(last_frame)
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


n = animation.find("N").shapes[0]
path = n.shapes[0]
bez = path.shape.value
ani_path = anutils.generate_path_appear(bez, write_start, write_end, 10)
n.shapes[0] = ani_path

n.transform.opacity.add_keyframe(0, 0, easing.Jump())
n.transform.opacity.add_keyframe(write_start, 100)
n.transform.opacity.add_keyframe(fade_start, 100)
n.transform.opacity.add_keyframe(fade_end, 0)

hand = animation.find("hand")
#pos = hand.transform.anchor_point.value
#hand.transform.position.value = pos - NVector(135, 40)
off = -bez.vertices[0] - NVector(135, 40)
hand.transform.position.add_keyframe(0, NVector(0, 0), easing.EaseIn())
anutils.follow_path(hand.transform.position, bez, write_start, write_end, 10, False, off)
hand.transform.position.add_keyframe(return_end, NVector(0, 0), easing.Sigmoid())


script.script_main(animation, formats=["tgs", "html"])
