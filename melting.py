import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


rays = animation.find("rays")
sun = animation.find("sun")
sunp = sun.shapes[0].shapes[0].position.value


def damp(p):
    r = (p.x - sunp.x) / (512 - sunp.x)
    return r


displacer = anutils.SineDisplacer(200, 24, 0, last_frame, 8, 1, 90)
displacer = anutils.DisplacerDampener(displacer, damp)


for ray in rays.find_all(lottie.objects.Path):
    displacer.animate_bezier(ray.shape)


n_rays = 4
for i in range(1, n_rays):
    g = animation.layers[0].insert_shape(0, lottie.objects.Group())
    g.transform.position.value = g.transform.anchor_point.value = sunp
    g.transform.rotation.value = i * 90 / (n_rays - 1)
    #if i % 2:
    g.transform.scale.value = NVector(100, -100)
    g.add_shape(rays)

#import pdb; pdb.set_trace(); pass

c1 = sun.shapes[1].shapes[0]
c1s = c1.size.value
c2 = sun.shapes[2].shapes[0]
c2s = c2.size.value

c1.size.add_keyframe(0, c1s, easing.Sigmoid())
c1.size.add_keyframe(last_frame/2, c2s, easing.Sigmoid())
c1.size.add_keyframe(last_frame, c1s)

c2.size.add_keyframe(0, c2s, easing.Sigmoid())
c2.size.add_keyframe(last_frame/2, c1s, easing.Sigmoid())
c2.size.add_keyframe(last_frame, c2s)


script.script_main(animation, formats=["tgs", "html"])
