import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie import NVector

name = "credits"

pull_end_base = 60
wait_end_base = pull_end_base + 70
exit_end_base = wait_end_base + 10
last_frame = exit_end_base + 40

animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


settings = [
    ["G",  0, 10],
    ["L", 20,  0],
    ["A", 10,  5],
    ["X", 25,  9],
]

for l, delay, d1 in settings:
    letter = animation.find(l)
    start_y = 182
    end_y = 340
    pull_start = delay
    pull_end = pull_end_base+delay
    wait_end = wait_end_base+d1
    exit_end = exit_end_base+d1
    position = letter.transform.position
    position.value = NVector(0, -start_y)
    if pull_start > 0:
        position.add_keyframe(0, NVector(0, -end_y))
    anutils.spring_pull(position, NVector(0, 0), pull_start, pull_end, 15, 7)
    position.add_keyframe(wait_end, position.keyframes[-1].start)
    position.add_keyframe(exit_end, NVector(0, end_y))

script.script_main(animation, formats=["tgs", "html"])
