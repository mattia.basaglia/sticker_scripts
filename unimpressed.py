import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie.utils.stripper import heavy_strip
from lottie import objects

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

animation.find("markers").transform.opacity.value = 0
center = animation.find("marker_rot").shapes[0].position.value
center.components.append(128)

yesno = "no"
#yesno = "yes"

if yesno == "no":
    axis = NVector(-1, 4, 1)
    angle = -20
else:
    axis = NVector(4, 0, -1)
    angle = 15

forward = anutils.DepthRotationDisplacer(
    center,
    0, last_frame/2, 10,
    axis,
    0, angle, 0,
    easing.Sigmoid()
)
backward = anutils.DepthRotationDisplacer(
    center,
    last_frame/2, last_frame, 10,
    axis,
    0, -angle, 0,
    easing.Sigmoid()
)


def comp_depth_group(group, comp, multiplier, offset, xref, exp):
    if isinstance(group, lottie.objects.Group):
        for c in group.shapes[:-1]:
            comp_depth_group(c, comp, multiplier, offset, xref, exp)
    elif isinstance(group, lottie.objects.Path):
        bez = group.shape.value
        for i in range(len(bez.vertices)):
            x = bez.vertices[i][comp]
            depth = (abs(x - xref) ** exp) * multiplier / 512 + offset
            bez.vertices[i].components.append(depth)
            din = abs(x + bez.in_tangents[i][comp] - xref) * multiplier / 512 + offset - depth
            bez.in_tangents[i].components.append(din)
            dout = abs(x + bez.out_tangents[i][comp] - xref) * multiplier / 512 + offset - depth
            bez.out_tangents[i].components.append(dout)

        forward.animate_bezier(group.shape)
        backward.animate_bezier(group.shape)


def comp_depth_layer(name, comp, multiplier, offset, xref, exp=1):
    for g in animation.find(name).shapes[:-1]:
        comp_depth_group(g, comp, multiplier, offset, xref, exp)


def dist_depth_group(group, multiplier, offset, pos):
    if isinstance(group, lottie.objects.Group):
        for c in group.shapes[:-1]:
            dist_depth_group(c, multiplier, offset, pos)
    elif isinstance(group, lottie.objects.Path):
        bez = group.shape.value
        for i in range(len(bez.vertices)):
            x = bez.vertices[i]
            depth = (x - pos).length * multiplier / 512 + offset
            bez.vertices[i].components.append(depth)
            din = (x + bez.in_tangents[i] - pos).length * multiplier / 512 + offset - depth
            bez.in_tangents[i].components.append(din)
            dout = (x + bez.out_tangents[i] - pos).length * multiplier / 512 + offset - depth
            bez.out_tangents[i].components.append(dout)

        forward.animate_bezier(group.shape)
        backward.animate_bezier(group.shape)


def dist_depth_layer(name, multiplier, offset, pos):
    for g in animation.find(name).shapes[:-1]:
        dist_depth_group(g, multiplier, offset, pos)


center = animation.find("marker_center").shapes[0].position.value

bb = animation.find("eye f").bounding_box()
comp_depth_layer("eye f", 0, 30, 50, bb.x1+30)

bb = animation.find("ear").bounding_box()
comp_depth_layer("ear", 0, 300, 60, bb.x1)
#fixed_depth_layer("horn f", 90)
#fixed_depth_layer("horn b", 150)


marker_neckc = animation.find("marker_neckc").shapes[0].position.value
comp_depth_layer("neck", 0, 300, 80, marker_neckc.x)
nlbb = animation.find("neck 1").bounding_box()
dist_depth_layer("neck 1", 350, 50, NVector((nlbb.x1+nlbb.x2)/2, nlbb.y2))

bb = animation.find("eye b").bounding_box()
#comp_depth_layer("eye b", 0, 400, 60, bb.x2)
dist_depth_layer("eye b", 360, 50, NVector(bb.x2, bb.y1*0.3+bb.y2*0.7))
#comp_depth_layer("head base", 0, 100, 128, center.x)
#fixed_depth_layer("head base", 128)



marker_muzzle = animation.find("marker_muzzle").shapes[0].position.value
comp_depth_layer("snout", 0, 350, -20, marker_muzzle.x)
comp_depth_layer("jaw", 0, 250, 0, marker_muzzle.x)


bb = animation.find("snout b").bounding_box()
if yesno == "no":
    #comp_depth_layer("snout b", 0, 350, -20, marker_muzzle.x)
    #comp_depth_layer("snout b", 0, 30, 60, bb.x1*0.7+bb.x2*0.3)
    comp_depth_layer("snout b", 0, -2, 70, bb.x1*0.7+bb.x2*0.3, 1.7)
else:
    #comp_depth_layer("snout b", 0, -2, 70, bb.x1*0.7+bb.x2*0.3, 1.7)
    comp_depth_layer("snout b", 0, -150, 70, bb.x1*0.7+bb.x2*0.3)

#def xz(name):
    #x = 1000
    #z = 0
    #for path in animation.find(name).find_all(objects.Path):
        #for v in path.shape.get_value(0).vertices:
            #if v.x < x:
                #x = v.x
                #z = v.z
    #print(x, z)

#xz("snout")
#xz("snout b")


bb = animation.find("head base").bounding_box()
comp_depth_layer("head base", 1, 200, 60, bb.y2)


bb = animation.find("horn f").bounding_box()
comp_depth_layer("horn f", 1, 50, 60, bb.y2)


bb = animation.find("horn b").bounding_box()
comp_depth_layer("horn b", 1, 50, 140, bb.y2)


script.script_main(animation, formats=["tgs", "html"], strip=heavy_strip)

