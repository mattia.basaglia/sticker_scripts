import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie import NVector

name = "this"
flip = False

point_start = 0
point_end = 40
point_away = 80
point_away_end = point_away + 20

nod_start = 10
nod_end = nod_start + 50
gaze_start = 20
gaze_reach = gaze_start + 7
gaze_return = point_away_end - 5
gaze_end = point_away_end

shake_start = 10
shake_end = shake_start + 30


last_frame = point_away_end
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

if flip:
    animation.layers[0].transform.scale.value = NVector(-100, 100)
    animation.layers[0].transform.position.value = NVector(512, 0)

hand = animation.find("hand")
hand_away = 150
hand_up = 30
hand.transform.position.value = NVector(0, hand_away)
anutils.spring_pull(hand.transform.position, NVector(0, hand_up), point_start, point_end, 30, 3)
#anutils.spring_pull(hand.transform.position, NVector(0, hand_away), point_away, point_away_end, 4, 3)

#hand.transform.position.add_keyframe(point_away-7, NVector(0, 40))
#hand.transform.position.add_keyframe(point_away, NVector(0, 20))
#hand.transform.position.add_keyframe(point_away_end, NVector(0, hand_away))
hand.transform.position.add_keyframe(point_away, NVector(0, hand_up))
hand.transform.position.add_keyframe(point_away_end, NVector(0, hand_away))


text = animation.find("text")
textflip = animation.find("textflip")
if flip:
    [textflip, text] = [text, textflip]
textflip.transform.opacity.value = 0
text.transform.opacity.value = 100
ang = 7
anutils.rot_shake(text.transform.rotation, NVector(-ang, ang), shake_start, shake_end, 10)


head = animation.find("dragon")
#head.transform.rotation.add_keyframe(nod_start, 0)
#head.transform.rotation.add_keyframe(nod_end, 0)
ang = 3
anutils.rot_shake(head.transform.rotation, NVector(ang, -ang), nod_start, nod_end, 3)

eyel = animation.find("eyel")
gazel = NVector(6, -3)
eyel.transform.position.add_keyframe(gaze_start, NVector(0, 0))
eyel.transform.position.add_keyframe(gaze_reach, gazel)
eyel.transform.position.add_keyframe(gaze_return, gazel)
eyel.transform.position.add_keyframe(gaze_end, NVector(0, 0))
eyer = animation.find("eyer")
gazer = NVector(7, -6)
eyer.transform.position.add_keyframe(gaze_start, NVector(0, 0))
eyer.transform.position.add_keyframe(gaze_reach, gazer)
eyer.transform.position.add_keyframe(gaze_return, gazer)
eyer.transform.position.add_keyframe(gaze_end, NVector(0, 0))


script.script_main(animation, formats=["tgs", "html"])
