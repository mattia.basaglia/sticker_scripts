import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 120
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


label = "memes"

labels = {
    g.name: g
    for g in animation.find("label").shapes[:-1]
}

angles = {
    "rip": 180,
    "noises": 135,
    "sfw": 90,
    "trains": 45,
    "memes": 0,
}

knob = animation.find("knob")

for lab, group in labels.items():
    if lab == label:
        knob.transform.rotation.add_keyframe(0, -angles[lab])
        knob.transform.rotation.add_keyframe(20, -angles[lab], easing.EaseIn())
        knob.transform.rotation.add_keyframe(70, 360-angles[lab]-45)
        anutils.spring_pull(knob.transform.rotation, 360-angles[lab], 70, last_frame)
    else:
        group.transform.opacity.value = 0


script.script_main(animation, formats=["tgs", "html"])
