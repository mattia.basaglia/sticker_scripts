import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


mc = animation.find("mouth concerned")
mc.transform.opacity.value = 0
mcbez = mc.shapes[0].shapes[0].shape.value

ms = animation.find("mouth scrunchy")
msbez = ms.shapes[0].shapes[0].shape.value

ms.shapes[0].shapes[0].shape.add_keyframe(0, msbez, easing.EaseIn())
ms.shapes[0].shapes[0].shape.add_keyframe(30, mcbez)
ms.shapes[0].shapes[0].shape.add_keyframe(50, mcbez, easing.EaseOut())
ms.shapes[0].shapes[0].shape.add_keyframe(60, msbez)


boop = animation.find("boop")
bb = boop.bounding_box()
boop.transform.anchor_point.value = boop.transform.position.value = bb.center()

boop.transform.opacity.add_keyframe(0, 50)
boop.transform.opacity.add_keyframe(5, 100)
boop.transform.opacity.add_keyframe(20, 100)
boop.transform.opacity.add_keyframe(40, 0)
boop.transform.opacity.add_keyframe(55, 0)
boop.transform.opacity.add_keyframe(60, 50)

boop.transform.scale.add_keyframe(0, NVector(50, 50))
anutils.spring_pull(boop.transform.scale, NVector(100, 100), 0, 20, 7, 4)
boop.transform.scale.add_keyframe(40, NVector(100, 100))
boop.transform.scale.add_keyframe(55, NVector(0, 0))
boop.transform.scale.add_keyframe(60, NVector(50, 50))


b0 = animation.find("bone 0")
b0.transform.rotation.add_keyframe(0, -10, easing.EaseOut())
b0.transform.rotation.add_keyframe(35, 50, easing.EaseIn())
b0.transform.rotation.add_keyframe(60, -10)

b1 = animation.find("bone 1")
b1.transform.rotation.add_keyframe(0, 10, easing.EaseOut())
b1.transform.rotation.add_keyframe(35, -50, easing.EaseIn())
b1.transform.rotation.add_keyframe(60, 10)

p1 = animation.find("pupil1")
p1.transform.rotation.add_keyframe(0, -15, easing.Sigmoid())
p1.transform.rotation.add_keyframe(35, 10, easing.Sigmoid())
p1.transform.rotation.add_keyframe(60, -15)

p1 = animation.find("pupil2")
p1.transform.rotation.add_keyframe(0, 0, easing.Sigmoid())
p1.transform.rotation.add_keyframe(35, 50, easing.Sigmoid())
p1.transform.rotation.add_keyframe(60, 0)


head = animation.find("ych head")
head.transform.rotation.add_keyframe(0, 3)
head.transform.rotation.add_keyframe(20, 0)
head.transform.rotation.add_keyframe(55, 0, easing.EaseOut())
head.transform.rotation.add_keyframe(60, 3)


script.script_main(animation, formats=["tgs", "html"])
