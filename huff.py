import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie import NVector

name = "huff"
duration = 3
last_frame = 120
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


head = animation.find("head")
head.transform.rotation.add_keyframe(0, -8)
head.transform.rotation.add_keyframe(50, 4)
head.transform.rotation.add_keyframe(last_frame, -8)


arm = animation.find("arm f")
arm.transform.rotation.add_keyframe(0, 0)
arm.transform.rotation.add_keyframe(last_frame/4, -2)
arm.transform.rotation.add_keyframe(last_frame/4*3, 2)
arm.transform.rotation.add_keyframe(last_frame, 0)


wingf = animation.find("wing f")
wingf.transform.rotation.add_keyframe(0, 0)
wingf.transform.rotation.add_keyframe(last_frame/3, 4)
wingf.transform.rotation.add_keyframe(last_frame/3*2, -6)
wingf.transform.rotation.add_keyframe(last_frame, 0)


wingb = animation.find("wing b")
wingb.transform.rotation.add_keyframe(0, 0)
wingb.transform.rotation.add_keyframe(25, 3)
wingb.transform.rotation.add_keyframe(85, -5)
wingb.transform.rotation.add_keyframe(last_frame, 0)


huff = animation.find("huff")
huff.transform.opacity.add_keyframe(0, 0)
huff.transform.opacity.add_keyframe(33, 0)
huff.transform.opacity.add_keyframe(50, 100)
huff.transform.opacity.add_keyframe(last_frame, 0)

huff.transform.position.add_keyframe(33, NVector(20, 10))
huff.transform.position.add_keyframe(last_frame, NVector(-5, -5))


script.script_main(animation, formats=["tgs", "html"])
