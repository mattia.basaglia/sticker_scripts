import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie import objects

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 20
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)
#animation = parse_svg_file("/tmp/drawing.svg")

tongue = animation.find("tongue")
tongue2 = animation.find("tongue2")
tongue2.transform.opacity.value = 0


for t, t2 in zip(tongue.find_all(objects.Path), tongue2.find_all(objects.Path)):
    v = t.shape.value
    t.shape.add_keyframe(0, v)
    t.shape.add_keyframe(last_frame/2, t2.shape.value)
    t.shape.add_keyframe(last_frame, v)


script.script_main(animation, formats=["tgs", "html"])
