import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


dragon = animation.find("dragon")
dragon.transform.anchor_point.value = NVector(256, 512)
dragon.transform.position.value = dragon.transform.anchor_point.value.clone()
dragon.transform.position.value.y += 32

bounce_start = 2
dragon.transform.scale.add_keyframe(0, NVector(100, 100), easing.Sigmoid())
dragon.transform.scale.add_keyframe(bounce_start, NVector(100, 90), easing.EaseIn())
dragon.transform.scale.add_keyframe(bounce_start+15, NVector(100, 110), easing.EaseOut())
dragon.transform.scale.add_keyframe(bounce_start+30, NVector(100, 100))
#bounce_end = bounce_start+30

blink_speed = 10
#derpyeyes = animation.find("derpyeyes")
#derpyeyes.transform.anchor_point.value = derpyeyes.transform.position.value = derpyeyes.bounding_box().center()
#derpyeyes.transform.scale.add_keyframe(bounce_end, NVector(100, 100))
#derpyeyes.transform.scale.add_keyframe(bounce_end+blink_speed*2, NVector(100, 0))
#derpyeyes.transform.scale.add_keyframe(bounce_end+blink_speed*3, NVector(100, 100))
eyelids_open = animation.find("eyelid_open").find_all(lottie.objects.Path)
eyelids_closed = animation.find("eyelid_closed").find_all(lottie.objects.Path)
blink_start = bounce_start+15
for eyelid_open, eyelid_closed in zip(eyelids_open, eyelids_closed):
    shape_open = eyelid_open.shape.value
    shape_closed = eyelid_closed.shape.value
    eyelid_open.shape.add_keyframe(bounce_start, shape_open)
    eyelid_open.shape.add_keyframe(bounce_start+13, shape_closed)
    eyelid_open.shape.add_keyframe(bounce_start+17, shape_closed)
    eyelid_open.shape.add_keyframe(bounce_start+30, shape_open)
blink_end = bounce_start + 30 + 30


animation.out_point = animation.layers[0].out_point = blink_end

script.script_main(animation, formats=["tgs", "html"], strip=lambda x: None)

