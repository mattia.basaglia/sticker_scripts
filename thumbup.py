import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

bone0 = animation.find("bone0")
bone1 = bone0.find("bone1")

fb = animation.find("foobar")
#import pdb; pdb.set_trace()
bone0.transform.rotation.add_keyframe(0, -15)
bone0.transform.rotation.add_keyframe(10, 0)
bone0.transform.rotation.add_keyframe(40, 0)
bone0.transform.rotation.add_keyframe(60, -15)

bone1.transform.rotation.add_keyframe(0, -15)
bone1.transform.rotation.add_keyframe(20, 0)
bone1.transform.rotation.add_keyframe(25, 2)
bone1.transform.rotation.add_keyframe(30, 0)
bone1.transform.rotation.add_keyframe(50, -10)
bone1.transform.rotation.add_keyframe(60, -15)


script.script_main(animation, formats=["tgs", "html"])
