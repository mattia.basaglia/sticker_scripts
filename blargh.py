import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
import random

name = "blargh"
last_frame = 15
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

rainbow = animation.find("rainbow")
displacer = anutils.MultiSineDisplacer([(300, 12), (91, 6)], 0, last_frame, 10, -1, 60)
for g in rainbow.shapes[:-1]:
    random.seed(1)
    displacer.animate_bezier(g.shapes[0].shape)


above = animation.find("above")
below = animation.find("below")
pos = [above.transform.position, below.transform.position]
anutils.shake(pos, 8, 8, 0, last_frame, 10)

for g in animation.find("eye f").shapes[:-1]:
    anutils.shake(g.transform.position, 5, 5, 0, last_frame, 10)

eyeb = animation.find("eye b")
anutils.shake(eyeb.shapes[0].transform.position, 2, 2, 0, last_frame, 10)
anutils.shake(eyeb.shapes[1].transform.position, 4, 4, 0, last_frame, 10)

script.script_main(animation, formats=["tgs", "html"])
