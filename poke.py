import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie import NVector

name = "poke"
duration = 3
last_frame = duration*60-1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

arm = animation.find("arm")
strokes = animation.find("strokes")


arm.transform.position.add_keyframe(0, NVector(150, 0))
arm.transform.position.add_keyframe(30, NVector(-120, -28))
arm.transform.position.add_keyframe(60, NVector(100, 20))
arm.transform.position.add_keyframe(90, NVector(50, 0))
arm.transform.position.add_keyframe(120, NVector(-120, -28))
arm.transform.position.add_keyframe(150, NVector(130, -40))
arm.transform.position.add_keyframe(last_frame, NVector(150, 0))

fade_start = 5
fade_end = 20
strokes.transform.opacity.add_keyframe(0, 0)
strokes.transform.opacity.add_keyframe(30, 0)
strokes.transform.opacity.add_keyframe(30 + fade_start, 100)
strokes.transform.opacity.add_keyframe(30 + fade_end, 0)
strokes.transform.opacity.add_keyframe(120, 0)
strokes.transform.opacity.add_keyframe(120 + fade_start, 100)
strokes.transform.opacity.add_keyframe(120 + fade_end, 0)


script.script_main(animation, formats=["tgs", "html"])
