import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 120
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

t = animation.layers[0].transform
t.anchor_point.value = NVector(512, 512)
t.position.value = NVector(512, 512)
#t.position.value += NVector(0, 20)
t.scale.value = NVector(95, 95)

tail = animation.find("tail")
tail.transform.rotation.value = -57
#tail.transform.position.value -= NVector(100, 0)
tail_shapes = tail.find_all(lottie.objects.Path)

start_pos = NVector(542.6, 284.5)
rx = 150
ry = 100


def displace1(t):
    x = math.sin((t/2+.25)*math.pi) - 1
    y = math.cos((t/2+.25)*math.pi)
    return NVector(x * rx, -y * ry)


def displace(t):
    if t < 0.5:
        return displace1(1-t*2)
    return displace1((t-0.5)*2)


displacer = anutils.FollowDisplacer(start_pos, 230, displace, 0, last_frame, 30, 0.7)

for s in tail_shapes:
    #s.shape.value.split_each_segment()
    #s.shape.value.split_each_segment()
    #s.shape.value.split_each_segment()
    displacer.animate_bezier(s.shape)

#l = tail.insert_shape(0, lottie.objects.Group())
#e = l.add_shape(lottie.objects.Ellipse())
#e.size.value = NVector(10, 10)
#e.position.value = start_pos
#l.add_shape(lottie.objects.Fill(NVector(1, 0, 0)))
#displacer.animate_point(e.position)


front = animation.find("front")
front.transform.rotation.add_keyframe(0, 15)
front.transform.rotation.add_keyframe(last_frame/2, -10)
front.transform.rotation.add_keyframe(last_frame, 15)

head = animation.find("head")
head.transform.rotation.add_keyframe(0, -20)
head.transform.rotation.add_keyframe(last_frame/2, 0)
head.transform.rotation.add_keyframe(last_frame, -20)


eyef = animation.find("eye f")
eyef.transform.position.value = eyef.transform.anchor_point.value = eyef.bounding_box().center()
eyef.transform.rotation.add_keyframe(0, -5)
eyef.transform.rotation.add_keyframe(last_frame/2, 5)
eyef.transform.rotation.add_keyframe(last_frame, -5)

eyeb = animation.find("eye b")
eyeb.transform.position.value = eyeb.transform.anchor_point.value = eyeb.bounding_box().center()
eyeb.transform.rotation.add_keyframe(0, -5)
eyeb.transform.rotation.add_keyframe(last_frame/2, 5)
eyeb.transform.rotation.add_keyframe(last_frame, -5)


script.script_main(animation, formats=["tgs", "html"])
