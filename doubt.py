import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]

bp_start = 20
fade_start = bp_start + 20
fade_end = fade_start + 30
unfade_start = fade_end + 80
unfade_end = unfade_start + 20
last_frame = unfade_end
print(last_frame)
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

doubt = animation.find("doubt")
truth = animation.find("truth")
lie = animation.find("lie")

i = -1
for msg in (truth, lie):
    msg.transform.opacity.add_keyframe(fade_start, 100)
    msg.transform.opacity.add_keyframe(fade_end, 0)
    msg.transform.opacity.add_keyframe(unfade_start, 0)
    msg.transform.opacity.add_keyframe(unfade_end, 100)
    msg.transform.position.add_keyframe(fade_end, NVector(0, 0))
    msg.transform.position.add_keyframe(unfade_start, NVector(0, i*32))
    msg.transform.position.add_keyframe(unfade_end, NVector(0, 0))
    i *= -1

button = animation.find("button")
button.transform.position.value = button.transform.anchor_point.value = button.bounding_box().center()
button.transform.scale.add_keyframe(bp_start, NVector(100, 100), easing.Sigmoid())
button.transform.scale.add_keyframe(bp_start+10, NVector(80, 80), easing.Sigmoid())
button.transform.scale.add_keyframe(bp_start+20, NVector(100, 100))

script.script_main(animation, formats=["tgs", "html"])
