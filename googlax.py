import os
import math
import random
import lottie
from lottie import objects
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector
from lottie.utils.font import FontStyle


name = os.path.splitext(os.path.basename(__file__))[0]
text_start = 60
text_time = 60
last_frame = text_start + text_time + 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)
text = "How to Rawr"
font = FontStyle('Nimbus Sans L', 25.5)
char_time = text_time / (len(text) + 1)


g = objects.Group()
animation.layers[0].insert_shape(0, g)

cursor = animation.find("path4625")
curbb = cursor.bounding_box()
g.transform.position.value = NVector(curbb.x1, curbb.y2)

cursor.transform.position.add_keyframe(0, NVector(0, 0), easing.Jump())

dx = 0
for i, c in enumerate(text):
    sh = g.add_shape(font.render(c))
    sh.transform.position.value.x = dx
    ntime = text_start + char_time * (i+1)
    dx += sh.next_x
    fill = sh.add_shape(objects.Fill(NVector(0, 0, 0)))
    fill.opacity.add_keyframe(0, 0, easing.Jump())
    fill.opacity.add_keyframe(ntime, 100, easing.Jump())
    cursor.transform.position.add_keyframe(ntime, NVector(dx+3, 0), easing.Jump())


for i in range(0, last_frame, 30):
    cursor.transform.opacity.add_keyframe(i, 0, easing.Jump())
    cursor.transform.opacity.add_keyframe(i+10, 100, easing.Jump())


script.script_main(animation, formats=["tgs", "html"])
