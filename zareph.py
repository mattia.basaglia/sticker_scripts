import os
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils

name = "zareph-knot"
duration = 1
last_frame = duration*60-1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

knot = animation.find("knot")
anutils.shake(knot.transform.position, 10, 20, 0, last_frame, 35)


script.script_main(animation, formats=["tgs", "html"])
