import os
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie import objects
from lottie.nvector import NVector

name = "rain"

fall_time = 20
fade_duration = 5

last_frame = fall_time + 1
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)

jittervals = [0.2, 0.4, 0, 0.7, 0.9, 0.3]

rain_layer = animation.find("rain")
old = rain_layer.shapes
rain_layer.shapes = [objects.TransformShape()]
for g in old:
    if not isinstance(g, objects.Group) or not g.shapes:
        continue

    line = g.shapes[0]
    if not isinstance(line, objects.Path):
        continue

    p1 = line.shape.value.vertices[0]
    p2 = line.shape.value.vertices[-1]

    if p1[1] > p2[1]:
        t = p2
        p1 = p2
        p2 = p1

    p1 = NVector(*p1)
    p2 = NVector(*p2)

    dropg = rain_layer.add_shape(objects.Group())
    droplet = dropg.add_shape(objects.Ellipse())
    fill = dropg.add_shape(objects.Fill(NVector(0.5372549019607843, 0.8, 0.9764705882352941)))
    droplet.size.value = [10, 10]
    #jitter = random.random()
    jitter = jittervals.pop()
    sp = p1*(1-jitter) + p2 * jitter
    hit_time = fall_time * (1-jitter)
    droplet.position.add_keyframe(0, sp)
    droplet.position.add_keyframe(hit_time, p2)
    droplet.position.add_keyframe(hit_time + 1, p1)
    droplet.position.add_keyframe(fall_time, sp)

    if hit_time < fade_duration:
        valstart = 100 * (hit_time/fade_duration)
        fill.opacity.add_keyframe(0, valstart)
        fill.opacity.add_keyframe(hit_time, 0)
        fill.opacity.add_keyframe(hit_time + 1, 0)
        fill.opacity.add_keyframe(hit_time + 1.1, 100)
        fill.opacity.add_keyframe(hit_time + fall_time - fade_duration, 100)
        fill.opacity.add_keyframe(last_frame, valstart)
    else:
        fill.opacity.add_keyframe(hit_time - fade_duration, 100)
        fill.opacity.add_keyframe(hit_time, 0)
        fill.opacity.add_keyframe(hit_time + 1, 0)
        fill.opacity.add_keyframe(hit_time + 1.1, 100)

    #droplet.position.add_keyframe(jitter + fall_time + 1, p1)


script.script_main(animation, formats=["tgs", "html"])
