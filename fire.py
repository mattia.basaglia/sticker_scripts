import os
import math
import random
import lottie
from lottie.parsers.svg import parse_svg_file
from lottie.utils import script
from lottie.utils import animation as anutils
from lottie.objects import easing
from lottie import NVector

name = os.path.splitext(os.path.basename(__file__))[0]
last_frame = 60
animation = parse_svg_file(os.path.join(os.path.dirname(__file__), "pic", name + ".svg"), 0, last_frame)


displacer0 = anutils.MultiSineDisplacer([(300, 16), (91, 6)], 0, last_frame, 10, 1)
displacer1 = anutils.MultiSineDisplacer([(200, 10), (300, 7), (91, 5)], 0, last_frame, 10, 1)

displacers = [displacer0, displacer1]

random.seed(0)
for g, displacer in zip(animation.find("fire").shapes, displacers):
    shape = g.shapes[0].shape
    if displacer is displacer1:
        shape.value.split_each_segment()
        shape.value.split_each_segment()

    fixed = [
        (i, v.clone())
        for i, v in enumerate(shape.value.vertices)
        if v.x <= 266
    ]
    displacer.animate_bezier(shape)
    for kf in shape.keyframes:
        for i, v in fixed:
            kf.start.vertices[i] = v
            if kf.end:
                kf.end.vertices[i] = v


script.script_main(animation, formats=["tgs", "html"])
